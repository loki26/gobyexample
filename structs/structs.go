package main

import "fmt"

type person struct {
	name string
	age int
}

func main() {

	fmt.Println(person{"Nicola", 20})

	fmt.Println(person{name: "Luigi", age: 30})

	fmt.Println(person{name: "Alessandro"})

	fmt.Println(&person{name: "Luca", age: 40})

	s := person{name: "Mario", age: 50}
	fmt.Println(s.name)

	sp := &s
	fmt.Println(sp.age)

	sp.age = 51
	fmt.Println(sp.age)
}