package main

import (
	"fmt"
	"time"
)

// This function is going to be used into a goroutine that will be notified by the channel passed as argument
func worker(done chan bool) {
	fmt.Print("working...")
	time.Sleep(time.Second)
	fmt.Println("done")

	done <- true
}

func main() {
	done := make(chan bool, 1)
	go worker(done)

	// Without this line the worked won't work
	// This will block everything until the worker is going
	<-done
}