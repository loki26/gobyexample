package main

import (
	"fmt"
	"time"
)

func main() {

	/* Suppose we want to limit our handling of incoming requests 
	\we will serve those requests off a channel of the same name*/
	requests := make(chan int, 5)
	for i := 1; i <= 5; i++ {
		requests <- i
	}
	close(requests)

	// Limiter channel, will receive a value every 200ms
	limiter := time.Tick(200 * time.Millisecond)

	// This way we block request, only receiving one each 200ms
	for req := range requests {
		<-limiter
		fmt.Println("request", req, time.Now())
	}

	/* We may want to allow short bursts of requests in our rate limiting scheme
	 while preserving the overall rate limit. This channel will allow bursts of up to 3 requests */
	burstyLimiter := make(chan time.Time, 3)


	// Fill up the channel in order to rapresent that we can manage 3 requests per time
	for i := 0; i < 3; i++ {
		burstyLimiter <- time.Now()
	}

	// Every 200ms we will try to add a new value to burstyLimiter, up to it's limit(3)
	go func() {
		for t := range time.Tick(200 * time.Millisecond) {
			burstyLimiter <- t
		}
	}()

	// Now we simulate to receive 5 requests, but 3 are managed simultaneously
	burstyRequests := make(chan int, 5)
	for i := 1; i <= 5; i ++ {
		burstyRequests <- i
	}
	close(burstyRequests)
	for req := range burstyRequests {
		<-burstyLimiter
		fmt.Println("request", req, time.Now())
	}
}