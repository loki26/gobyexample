package main

import "fmt"
import "time"
import "sync/atomic"
import "runtime"

func main() {

	// Always positive counter
	var ops uint64 = 0

	/* We want to simulate concurrent updates, launching 50 goroutines
	Those goroutines will update counter every millisecond*/
	for i := 0; i < 50; i++ {
		go func() {
			for {
				// We use this function in order to increment counter using a pointer to the variable
				atomic.AddUint64(&ops, 1)

				// This way we will allow to other routines to proceed
				runtime.Gosched()
			}
		}()
	}

	// Waiting a second in order to let some goroutines to operate some increments
	time.Sleep(time.Second)
	
	/* This way we can read the counter while it's being updated from goroutines
	With LoadUint64 we create a copy to store into the variable using the pointer*/
	opsFinal := atomic.LoadUint64(&ops)
	fmt.Println("ops:", opsFinal)
}