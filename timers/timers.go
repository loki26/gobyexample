package main
import (
	"fmt"
	"time"
)

func main() {

	/* Timers represent a single event in the future.
	This tell the time how long this have to wait and provide a channel that will be notified at
	 that time. In this way timer will wait 2 seconds*/
	timer1 := time.NewTimer(2 * time.Second)

	// <-timer1.c blocks on the timer's channel c until it send a value indicating that the timer is fired
	<-timer1.C
	fmt.Println("Timer 1 fired")

	// It's nice to have timer instead of Sleep when you stop the timer before is firing
	timer2 := time.NewTimer(time.Second)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 fired")
	}()
	stop2 := timer2.Stop()
	if stop2 {
		fmt.Println("Timer 2 stopped")
	}

	time.Sleep(2 * time.Second)
}