package main

import (
	"fmt"
	"math/rand"
	"sync/atomic"
	"time"
)

/* This time we will manage state with a single goroutine. This will garantee that data would not
be corrupted from concurrent accesses.
In order to read state, goroutines should send messages at the goroutine that holds the state map
to get a response.
Those 2 structs encapsulate those requests and contains resp field to send response from the goroutine
that is held*/
type readOp struct {
	key int
	resp chan int
}
type writeOp struct {
	key int
	val int
	resp chan bool
}

func main() {

	// There we will hold how much ops will be performed
	var ops int64 = 0

	// Those 2 channels should be used from goroutines in order to perform read and write for the state
	reads := make(chan *readOp)
	writes := make(chan *writeOp)

	/* This goroutine will hold the state map, but it will be visible only from this goroutine
	 This goroutine will perform a select on the 2 channels and will send a response to the requests
	 This goroutine will manage the requests and will send a resp on the resp channel 
	 in order to tell that the request is successful
	 For the read we get the value and for the write we get the success of the operation
	*/
	go func() {
		var state = make(map[int]int)
		for {
			select {
			case read := <-reads:
				read.resp <- state[read.key]
			case write := <- writes:
				state[write.key] = write.val
				write.resp<- true
			}
		}
	}()

	/* Starting 100 goroutines that will execute reading requests of the state into the goroutine 
	that hold the state map with the channel reads. Each request should be axecuted through the readOp
	struct, sent on channel reads and waiting for the response on the resp channel*/
    for r := 0; r < 100; r++ {
        go func() {
            for {
                read := &readOp{
                    key:  rand.Intn(5),
					resp: make(chan int),
				}
                reads<- read
                <-read.resp
                atomic.AddInt64(&ops, 1)
            }
        }()
    }

	/* We star other 10 goroutines that will execute writes using a similar approach*/
    for w := 0; w < 10; w++ {
        go func() {
            for {
                write := &writeOp{
                    key:  rand.Intn(5),
                    val:  rand.Intn(100),
					resp: make(chan bool),
				}
                writes<- write
                <-write.resp
                atomic.AddInt64(&ops, 1)
            }
        }()
    }

	// Let's wait for a second in order to let goroutines execute some operations
	time.Sleep(time.Second)

	// There we read value from ops variable
	opsFinal := atomic.LoadInt64(&ops)
	fmt.Println("ops:", opsFinal)
}