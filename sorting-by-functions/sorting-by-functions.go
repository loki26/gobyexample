package main

import "sort"
import "fmt"

// This is an alyas for the build in type []string
type ByLength []string

/* Implementing interface  sort.Interface implementing functions Len Less and Swap in order to use
 Sort function that is generic for the package sort
*/
func (s ByLength) Len() int {
	return len(s)
}
func (s ByLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}

// Declared those functions we can use those performing a cast on fruits array 
// calling the funciton Sort
func main() {
	fruits := []string{"pesca", "banana", "kiwi"}
	sort.Sort(ByLength(fruits))
	fmt.Println(fruits)
}