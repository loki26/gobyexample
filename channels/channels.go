package main

import "fmt"

func main() {

	// Creating a new channel. Channel must be declared with allowed type to convey(veicolare in italiano)
	messages := make(chan string)

	// Sending "ping" via channel
	go func() { messages <- "ping" }()

	// Receiving values from channel messages
	msg := <- messages
	fmt.Println(msg)
}