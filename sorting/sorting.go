package main

import "fmt"
import "sort"

func main() {
	// Sort methods are build in for types in go. It doesn't need pointers and do not return new slices
	// This modify the slice passed
	strs := []string{"c", "a", "b"}
	sort.Strings(strs)
	fmt.Println("Strings:", strs)

	ints :=[]int{7, 2, 4}
	sort.Ints(ints)
	fmt.Println("Ints:	", ints)

	// We can determinate if a slice is yet ordinated
	s := sort.IntsAreSorted(ints)
	fmt.Println("Sorted:", s)
}