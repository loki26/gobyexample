package main

import "fmt"

func main() {

	// Channel that can buffer up to 2 values
	messages := make(chan string, 2)

	// Sending 2 values via buffer
	messages <- "buffered"
	messages <- "channel"

	// Getting values from the buffered channel
	fmt.Println(<-messages)
	fmt.Println(<-messages)
}