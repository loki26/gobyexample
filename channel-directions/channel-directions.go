package main

import "fmt"

// Ping functions only accepts a channel for sending values
func ping(pings chan<- string, msg string) {
	pings <- msg
}

// Pong function accept 2 channel, 1 is for receiving(pings) and 1 is for sending(pongs)
func pong(pings <-chan string, pongs chan<- string) {
	msg := <- pings
	pongs <- msg
}

func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)
}