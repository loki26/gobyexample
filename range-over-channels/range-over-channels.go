package main

import "fmt"

func main() {

	queue := make(chan string, 2)
	queue <- "one"
	queue <- "two"
	close(queue)

	// This will iterate over the closed queue (that still contains elements)
	for elem := range queue {
		fmt.Println(elem)
	}
}