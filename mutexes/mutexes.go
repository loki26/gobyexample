package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)

func main() {

	var state = make(map[int]int)

	// Mutex will sync the access to the state map
	var mutex = &sync.Mutex{}

	// This will work as counter of operations performed on the state map
	var ops int64 = 0

	// We start 100 goroutines that should execute reading on state map
	for r := 0; r < 100; r++ {
		go func() {
			total := 0
			for {

				/* For each reading on map we choose a random key.
				we use the Lock function in order to have exclusive control over the map
				We read and then we unlock the map, then perform an incremente*/
				key := rand.Intn(5)
				mutex.Lock()
				total += state[key]
				mutex.Unlock()
				atomic.AddInt64(&ops, 1)

				/* In order to be sure that this goroutine doesn't block the scheduler we let him
				take control after each operation with runtime.Gosched()
				This function is implicitly called with time.Sleep or on channels but this time we 
				should do this manually*/
				runtime.Gosched()
			}
		}()
	}

	// There we will start 10 goroutines to simulate writing with the same pattern used before
	for w := 0; w < 10; w++ {
		go func() {
			for {
				key := rand.Intn(5)
				value := rand.Intn(100)
				mutex.Lock()
				state[key] = value
				mutex.Unlock()
				atomic.AddInt64(&ops, 1)
				runtime.Gosched()
			}
		}()
	}

	time.Sleep(time.Second)

	opsFinal := atomic.LoadInt64(&ops)
	fmt.Println("ops:", opsFinal)

	mutex.Lock()
	fmt.Println("state:", state)
	mutex.Unlock()
}