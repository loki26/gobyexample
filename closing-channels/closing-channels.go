package main

import "fmt"

func main() {
	// Use of jobs channel in order to communicate with a goroutine
	jobs := make(chan int, 5)
	done := make(chan bool)

	// Worker goroutine, repeat until it gets every jobs
	// more value is false if jobs channel has no more jobs
	go func() {
		for {
			j, more := <-jobs
			if more {
				fmt.Println("received job", j)
			} else {
				fmt.Println("received all jobs")
				done <- true
				return
			}
		}
	}()

	for j := 1; j <= 3; j++ {
		jobs <- j
		fmt.Println("sent job", j)
	}
	close(jobs)
	fmt.Println("sent all jobs")

	// This way we are waiting the worker to be completed with synchronization approach
	<-done
}