package main

import (
	"fmt"
	"time"
)

func main() {

	/* Supposing we are executing an external call that return its result on c1 after 2 seconds
	Channel is buffered so the send in go routine is nonblocking
	*/
	c1 := make(chan string, 1)
	go func() {
		time.Sleep(2 * time.Second)
		c1 <- "result 1"
	}()

	// Select implemented to timeout the c1 channel if it takes more than 1s in responding
	select {
	case res := <-c1:
		fmt.Println(res)
	case <-time.After(1 * time.Second):
		fmt.Println("timeout 1")
	}

	c2 := make(chan string, 1)
	go func() {
		time.Sleep(2 * time.Second)
		c2 <- "result 2"
	}()
	select {
	case res := <-c2:
		fmt.Println(res)
	case <-time.After(3 * time.Second):
		fmt.Println("timeout 2")
	}
}