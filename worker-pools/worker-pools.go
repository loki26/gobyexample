package main

import (
	"fmt"
	"time"
)

// Worker in order to run several concurrent instances
func worker( id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", id, "started job", j)
		time.Sleep(time.Second)
		fmt.Println("worker", id, "finished job", j)
		results <- j * 2
	}
}

func main() {

	const numJobs = 5
	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)

	// This will start up 3 workers that won't work because of jobs pool empty
	for w := 1; w <= 3; w++ {
		go worker(w, jobs, results)
	}

	// There we send 5 jobs in the channel and then we close the channel because of no more jobs
	for j := 1; j <= numJobs; j++ {
		jobs<- j
	}
	close(jobs)

	/* We collect the results of the workers with synchronous mode
	This ensures that all the worker goroutines have finished
	Is possible to use even WaitGroup https://gobyexample.com/waitgroups
	*/
	for a := 1; a <= numJobs; a++ {
		<-results
	}
}