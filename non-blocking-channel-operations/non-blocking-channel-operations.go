package main

import "fmt"

func main() {
	messages := make(chan string)
	signals := make(chan bool)

	/* Non blocking receive, if a value is available on messages then select will make the <-messages case
	If in messages there's nothing to get, then default case will be executed*/
	select {
	case msg := <-messages:
		fmt.Println("received message", msg)
	default:
		fmt.Println("no message received")
	}

	// Non blocking send work similarly. msg can't be send to messages channel 
	// because the channel has no buffer and there's no receiver
	msg := "hi"
	select {
	case messages<- msg:
		fmt.Println("sent message", msg)
	default:
		fmt.Println("no message sent")
	}

	// Is possible to use multiple cases abode the default clause to implement
	// a multi-way non-blocking select. There we attempt a non blocking receives
	select {
	case msg := <-messages:
		fmt.Println("received message", msg)
	case sig := <-signals:
		fmt.Println("received signal", sig)
	default:
		fmt.Println("no activity")
	}
}